from colorama import Fore, Back, Style
from html.parser import HTMLParser
from io import StringIO
import os
import html
import requests
import subprocess
import sys
import time
import getopt


output_notifications = False
output_file = False
output_terminal = False
try:
    long_options = ["notifications", "file", "terminal"]
    values = []
    arguments, values = getopt.getopt(sys.argv[1:], "", long_options)

    for currentArgument, currentValue in arguments:
        print(currentArgument)
        if currentArgument == "--notifications":
            output_notifications = True
        elif currentArgument == "--file":
            output_file = True
        elif currentArgument == "--terminal":
            output_terminal = True

except getopt.error as err:
    print(str(err))

if len(sys.argv) == 1 or len(values) == 0:
    print("Usage:")
    print(
        f"     {sys.argv[0]} [--notifications] [--file] [--terminal] "
        "OWNCAST_CHAT_URL"
    )
    print(
        os.linesep+"Get the owncast chat url by looking at web request from "
        "/embed/chat/readwrite."
    )
    print("It will look something like this: ")
    print("https://live.example.com/api/chat?accessToken=SOME_RANDOM_TOKEN")
    exit(1)
elif len(arguments) == 0:
    output_terminal = True
    print("No output specified, assuming ('--terminal')")


chat_url = values[0]
chat_log_output_filename = "chatlog.txt"


class MLStripper(HTMLParser):
    def __init__(self):
        super().__init__()
        self.reset()
        self.strict = False
        self.convert_charrefs = True
        self.text = StringIO()

    def handle_starttag(self, tag, attributes):
        for attr in attributes:
            # write the 'alt' attribute if present
            if attr[0] == "alt":
                self.text.write(attr[1])

    def handle_data(self, d):
        self.text.write(d)

    def get_data(self):
        return self.text.getvalue()


def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()


history = {}

while True:
    r = requests.get(chat_url)

    chatlog = []
    chatlog_terminal = []
    for i in r.json():
        new_message = False
        if (not i['id'] in history.keys()):
            new_message = True
            first_seen = time.time()
            history[i['id']] = {"id": i['id'], "first_seen": first_seen}

        username = ""
        if "user" in i:
            username = "{}:".format(i['user']['displayName'])
        message = strip_tags(html.unescape(i['body']))

        if new_message or (time.time()-history[i['id']]['first_seen'] < 10):
            message_template = Fore.BLACK + Back.WHITE + "{}  {}" + Style.RESET_ALL + os.linesep
            if new_message and output_notifications:
                subprocess.run(
                    ['notify-send', '--icon=chat', '--category=im.received', username, message])
        else:
            message_template = Fore.WHITE + Back.BLACK + "{}" + Style.RESET_ALL + "  {}" + os.linesep
        message_plaintext_template = "{}: {}"

        terminal_line = message_template.format(username, message)
        plaintext_line = message_plaintext_template.format(
            username, message)
        chatlog.extend([plaintext_line])
        chatlog_terminal.extend([terminal_line])

    if output_terminal:
        # clear screen
        print(chr(27) + "[2J")
        print("\n".join(chatlog_terminal))

    if output_file:
        chatlog_file = open(chat_log_output_filename, "w")
        chatlog_file.write("\n".join(chatlog)+"\n")
        chatlog_file.close()

    time.sleep(2)
